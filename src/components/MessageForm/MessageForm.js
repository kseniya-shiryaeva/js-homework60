import React from 'react';
import './MessageForm.css';

const MessageForm = ({message, onSubmitMessage, onInputChange}) => {
    return (
        <form className="MessageForm" onSubmit={onSubmitMessage}>
            <label htmlFor="author">Author</label>
            <input type="text" id="author" name="author" value={message.author} onChange={onInputChange}/>
            <label htmlFor="message">Message</label>
            <textarea name="message" id="message" value={message.message} onChange={onInputChange}>
                {message.message}
            </textarea>
            <button type="submit">Send</button>
        </form>
    );
};

export default MessageForm;