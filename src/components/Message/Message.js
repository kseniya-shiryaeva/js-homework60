import React from 'react';
import './Message.css';

const Message = ({datetime, author, message}) => {
    return (
        <div className="Message">
            <p className="message-head">
                <span className="author">{author}</span>
                <span className="date">{new Date(datetime).toLocaleString()}</span>
            </p>
            <div className="message-body">{message}</div>
        </div>
    );
};

export default Message;