import './App.css';
import {useCallback, useEffect, useState} from "react";
import axios from "axios";
import Message from "./components/Message/Message";
import MessageForm from "./components/MessageForm/MessageForm";

function App() {
    const [messages, setMessages] = useState(null);
    const [lastTime, setLastTime] = useState(null);
    const [message, setMessage] = useState({
        message: '',
        author: ''
    });

    const fetchData = useCallback(async () => {
        let url = 'http://146.185.154.90:8000/messages';
        if (lastTime) {
            url += '?datetime=' + lastTime;
        }

        const request = await axios.get(url);

        if (request.data.length > 0) {
            if (messages) {
                setMessages(prev => ([
                    ...prev,
                    ...request.data
                ]));
            } else {
                setMessages(request.data);
            }

            setLastTime(request.data[request.data.length - 1].datetime);
        }
    }, []);

    useEffect(() => {
        setInterval(() => fetchData(), 5000);
    }, []);

    let content = null;

    if (messages) {
        content = messages.map(item => {
            return <Message message={item.message} author={item.author} datetime={item.datetime} key={Math.random()}/>
        });
    }

    const onInputChange = e => {
        const {name, value} = e.target;

        setMessage(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addMessage = async () => {
        return await axios.post('http://146.185.154.90:8000/messages', {'message': message.message, 'author': message.author});
    };

    const onSubmitMessage = e => {
        e.preventDefault();

        addMessage().finally(
            setMessage({
                message: '',
                author: ''
            })
        );
    };

    return (
        <div className="App">
            <MessageForm message={message} onSubmitMessage={e => onSubmitMessage(e)}
                         onInputChange={e => onInputChange(e)}/>
            <div className="messages">
                {content}
            </div>
        </div>
    );
}

export default App;
